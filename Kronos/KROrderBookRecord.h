//
//  KROrderBookRecord.h
//  Kronos
//

#pragma once

#include <iostream>
#include <string>

using namespace std;

typedef struct KROrderBookRecord {

    double timestamp;
    
    string order_operator;
    
    double price;
    
    double quantity;
    
} KROrderBookRecord;
