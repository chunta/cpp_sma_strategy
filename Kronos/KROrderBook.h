//
//  KROrderBook.h
//  Kronos

using namespace std;

#include <stdio.h>
#include "KROrderBookRecord.h"

class KROrderBook {
    
public:
    KROrderBook();
    
    void update(KROrderBookRecord book_record);
};
