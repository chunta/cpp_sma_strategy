//
//  KROrderFileParser.h
//  Kronos
//

#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "KROrderBookRecord.h"

using namespace std;

typedef enum {
  KROrderFileOption_CSV,
  KROrderFileOption_JSON,
  KROrderFileOption_XML
} KROrderFileOption;

class KROrderFileParser {

    public:
        KROrderFileParser();

        std::vector<KROrderBookRecord> paserFile(string filePath, KROrderFileOption option);
};
