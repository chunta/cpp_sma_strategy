//
//  KROrderFileParser.cpp
//  Kronos
//

#include "KROrderFileParser.h"
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <string.h>

KROrderFileParser::KROrderFileParser() {
    
}

vector<KROrderBookRecord> KROrderFileParser::paserFile(string filePath, KROrderFileOption option) {
    
    cout << "start parsing ...." << endl;
    vector<KROrderBookRecord> result;
    if (option == KROrderFileOption_CSV) {
        
        ifstream file(filePath);
        if (file.is_open()) {
            string line;
            while (getline(file, line)) {
                
                //Tokenizing each row of record
                istringstream ss(line);
                string token;
                char delim = ',';
                vector<string> tokens;
                cout << "[start]" << endl;
                int fieldCount = 0;
                KROrderBookRecord book_record;
                while(std::getline(ss, token, delim)) {
                    switch (fieldCount) {
                        case 0:
                            book_record.timestamp = stod(token);
                            break;
                        case 1:
                            book_record.order_operator = token;
                            break;
                        case 2:
                            book_record.price = stod(token);
                            break;
                        case 3:
                            book_record.quantity = stod(token);
                            break;
                        default:
                            break;
                    }
                    fieldCount++;
                }
                result.push_back(book_record);
                cout << "[end]" << endl;
            }
            file.close();
        }
    }
    cout << result.size() << endl;
    cout << "end parsing" << endl;
    return result;
}
