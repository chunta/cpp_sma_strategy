//
//  KROrder.h
//  Kronos
//

#pragma once

#include <iostream>
#include <string>

using namespace std;

typedef struct KROrderPair {
    
    double price;
    
    double quantity;
    
} KROrder;
