
#include "KRSMAStrategy.h"
#include "KROrderFileParser.h"

int main() {
    
    KROrderFileParser parser;
    vector<KROrderBookRecord> book_orders = parser.paserFile("btc_usdt_log.csv", KROrderFileOption_CSV);
    
    KROrderBook order_book;
    KRSMAStrategy strategy(order_book, 100000);
    
    return 1;
}
