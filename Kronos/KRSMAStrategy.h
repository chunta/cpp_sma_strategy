//
//  KRSMAStrategy.h
//  Kronos
//

#pragma once

#include "KROrderBook.h"

using namespace std;

class KRSMAStrategy {
    
public:
    KRSMAStrategy(KROrderBook record_book, int cash);
    
    void next();
    
    void buy();
    
    void sell();
    
    void update();
    
    void dump();
};
